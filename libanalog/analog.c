#include <stdio.h>
#include "analog.h"
#include "colors.h"

ANALOG_CONFIG_filter_t* ANALOG_CONFIG_filter = NULL;
char* ANALOG_CONFIG_default_id = NULL;
FILE* ANALOG_CONFIG_default_target = NULL;

#ifndef ANALOG_DISABLE_DEFAULT_LOG_TYPES
  ANALOG_DEFINE_LOG_TYPE(debug, ANALOG_ANSI_FG_BRIGHT_MAGENTA, ANALOG_ANSI_DEFAULT);
  ANALOG_DEFINE_LOG_TYPE(error, ANALOG_ANSI_FG_BRIGHT_RED, ANALOG_ANSI_FG_BRIGHT_YELLOW);
  ANALOG_DEFINE_LOG_TYPE(info, ANALOG_ANSI_FG_BRIGHT_CYAN, ANALOG_ANSI_DEFAULT);
  ANALOG_DEFINE_LOG_TYPE(warn, ANALOG_ANSI_FG_BRIGHT_YELLOW, ANALOG_ANSI_DEFAULT)
#endif

#ifdef ANALOG_ENV_SUPPORT
  #include <stdlib.h>
  #include <string.h>

  const char* ANALOG_getenvcolor(const char* log_type, const char* prefix, const char* fallback_color) {
    size_t log_type_len = strlen(log_type);
    size_t prefix_len = strlen(prefix);

    size_t envname_len = prefix_len + log_type_len + 1;
    char* envname = malloc(sizeof(char) * (envname_len));

    strncpy(envname, prefix, prefix_len);
    strncpy(envname + prefix_len, log_type, log_type_len);

    envname[envname_len - 1] = '\0';

    char* value = getenv(envname);
    if (value == NULL) return fallback_color;
    return value;
  }
#endif

int ANALOG_base_log(char* id, FILE* target, char* log_type, char* color, char* text_color, const char *format, va_list va_args) {
  if (ANALOG_CONFIG_filter != NULL && !ANALOG_CONFIG_filter(log_type)) return 0;
  if (target == NULL) target = stderr;
  if (id == NULL) id = "log";

  fprintf(target, ANALOG_ANSI_RESET "[" ANALOG_ANSI_FG_CYAN);
  fprintf(target, id);
  fprintf(target, ANALOG_ANSI_RESET " ");

  #ifdef ANALOG_ENV_SUPPORT
    fprintf(target, ANALOG_getenvcolor(log_type, ANALOG_ENV_COLOR_PREFIX, color));
  #else
    fprintf(target, color);
  #endif

  fprintf(target, log_type);
  fprintf(target, ANALOG_ANSI_RESET "] ");
  
  #ifdef ANALOG_ENV_SUPPORT
    fprintf(target, ANALOG_getenvcolor(log_type, ANALOG_ENV_TEXT_COLOR_PREFIX, text_color));
  #else
    fprintf(target, text_color);
  #endif

  int result = vfprintf(target, format, va_args);

  fprintf(target, ANALOG_ANSI_RESET);
  return result;
}