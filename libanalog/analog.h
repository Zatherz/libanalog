#ifndef ANALOG_H
#define ANALOG_H
#include <stdbool.h>
#include <stdarg.h>
#include <stdio.h>

#ifdef ANALOG_ENV_SUPPORT
	#define ANALOG_ENV_COLOR_PREFIX "ANALOG_COLOR_"
	#define ANALOG_ENV_TEXT_COLOR_PREFIX "ANALOG_TEXT_COLOR_"
#endif

#define ANALOG_DEFINE_LOG_TYPE(type, color, text_color) int ANALOGf_ ## type(const char* format, ...) { \
		                                      va_list va_args; \
		                                      int result; \
		                                      \
		                                      va_start (va_args, format); \
		                                      result = ANALOGitfv_ ## type(ANALOG_CONFIG_default_id, ANALOG_CONFIG_default_target, format, va_args); \
		                                      va_end (va_args); \
		                                      \
		                                      return result; \
		                                    } \
		                                    int ANALOGtf_ ## type(FILE* target, const char* format, ...) { \
		                                      va_list va_args; \
		                                      int result; \
		                                      \
		                                      va_start (va_args, format); \
		                                      result = ANALOGitfv_ ## type(ANALOG_CONFIG_default_id, target, format, va_args); \
		                                      va_end (va_args); \
		                                      \
		                                      return result; \
		                                    } \
		                                    int ANALOGif_ ## type(char* id, const char *format, ...) { \
		                                      va_list va_args; \
		                                      int result; \
		                                      \
		                                      va_start (va_args, format); \
		                                      result = ANALOGitfv_ ## type(id, ANALOG_CONFIG_default_target, format, va_args); \
		                                      va_end (va_args); \
		                                      \
		                                      return result; \
		                                    } \
		                                    int ANALOGitf_ ## type(char* id, FILE* target, const char *format, ...) { \
		                                      va_list va_args; \
		                                      int result; \
		                                      \
		                                      va_start (va_args, format); \
		                                      result = ANALOGitfv_ ## type(id, target, format, va_args); \
		                                      va_end (va_args); \
		                                      \
		                                      return result; \
		                                    } \
		                                    \
		                                    int ANALOGitfv_ ## type(char* id, FILE* target, const char *format, va_list va_args) { \
		                                      return ANALOG_base_log(id, target, #type, color, text_color, format, va_args); \
		                                    }

#define ANALOG_DECLARE_LOG_TYPE(type) int ANALOGf_ ## type(const char* format, ...);\
	                                  int ANALOGtf_ ## type(FILE* target, const char* format, ...);\
		                              int ANALOGif_ ## type(char* id, const char *format, ...); \
		                              int ANALOGitf_ ## type(char* id, FILE* target, const char *format, ...); \
		                              int ANALOGitfv_ ## type(char* id, FILE* target, const char *format, va_list va_args);
               
#ifdef ANALOG_ENV_SUPPORT
	const char* ANALOG_getenvcolor(const char* log_type, const char* prefix, const char* fallback_color);
#endif
int ANALOG_base_log(char* id, FILE* target, char* log_type, char* color, char* text_color, const char *format, va_list va_args);

#ifndef ANALOG_DISABLE_DEFAULT_LOG_TYPES
	// colors are specified in ANALOG_DEFINE_LOG_TYPE (in .c file(s))
	ANALOG_DECLARE_LOG_TYPE(debug);
	ANALOG_DECLARE_LOG_TYPE(error);
	ANALOG_DECLARE_LOG_TYPE(info);
	ANALOG_DECLARE_LOG_TYPE(warn);
#endif

typedef bool ANALOG_CONFIG_filter_t(char* log_type);

extern ANALOG_CONFIG_filter_t* ANALOG_CONFIG_filter;
extern char* ANALOG_CONFIG_default_id;
extern FILE* ANALOG_CONFIG_default_target;

#endif//ANALOG_H