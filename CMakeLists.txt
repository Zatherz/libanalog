cmake_minimum_required (VERSION 3.10)
project(libanalog_root)

set(IGNORE_UNUSED_VARS "${ANALOG_ENV_SUPPORT} ${ANALOG_DISABLE_DEFAULT_LOG_TYPES}")

subdirs(libanalog test)
