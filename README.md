# libanalog, the akit logger

libanalog is a very simple (but very powerful) C logger, made primarily for use by akit utilities, but suitable for many other applications.

## Building

libanalog uses CMake as its build system.

```bash
mkdir build
cd build
cmake ..
make
```

A test executable will be ran automatically.

## Normal usage

libanalog comes with four log types: `info`, `debug`, `warn` and `error`. By default, all of them will print no matter what.

Each log type has five printf-like functions (`X` is the log type):

* `ANALOGf_X` - one argument, `const char* format` + varargs
* `ANALOGif_X` - two arguments, `char* id` and `const char* format` + varargs
* `ANALOGtf_X` - two arguments, `FILE* target` and `const char* format` + varargs
* `ANALOGitf_X` - three arguments, `char* id`, `FILE* target` and `const char* format` + varargs
* `ANALOGitfv_X` - four arguments, `char* id`, `FILE* target`, `const char* format` and `va_list va_args`

Usually, you will only need the first two. To write to a file, or to stdout instead of the default stderr, you will usually employ `ANALOGtf_X` and `ANALOGitf_X`. For more advanced usage (for example when you want to make a wrapper function), you may sometimes use `ANALOGitfv_X`.

Example:

```c
int main(void) {
    ANALOGf_info("This is an info line.\n");
    ANALOGf_debug("This is a debug line.\n");
    ANALOGf_warn("This is a warn line.\n");
    ANALOGf_error("This is an error line.\n");

    ANALOGif_info("some_component", "This is an info line with a custom ID.\n");
    ANALOGtf_info(stdout, "This is an info line that writes to stdout instead of stderr.\n");
    ANALOGitf_info("some_component", stdout, "This is an info line with a custom ID that writes to stdout instead of stderr.\n");

    wrapper("This is an info line with a custom ID written to stdout instead of stderr through a wrapper.\n");
}

int wrapper(const char* format,
	va_list va_args; // forward the variable arguments
    int result;
      
    va_start (va_args, format);
    result = ANALOGitfv_info("some_other_component", stdout, format, va_args);
    va_end (va_args);
     
    return result;
}
```

Note that all of the log functions are printf-like, therefore you can make use of placeholders:

```c
int main(void) {
	ANALOGf_info("10: %d, 'a': %c, SSIZE_MAX: %zu\n", 10, 'a', SSIZE_MAX);
}
```

## Colors

libanalog comes with a set of macros for ANSI colors. All of these macros will return `""` (an empty string) on Windows, but will return valid ANSI color escape codes for other compilation targets. Surround versions of macros surround their argument with the wanted color and a reset.

### Foreground

* ANALOG_ANSI_FG_BLACK
* ANALOG_ANSI_FG_RED
* ANALOG_ANSI_FG_GREEN
* ANALOG_ANSI_FG_YELLOW
* ANALOG_ANSI_FG_BLUE
* ANALOG_ANSI_FG_MAGENTA
* ANALOG_ANSI_FG_CYAN
* ANALOG_ANSI_FG_WHITE
* ANALOG_ANSI_FG_BRIGHT_BLACK
* ANALOG_ANSI_FG_BRIGHT_RED
* ANALOG_ANSI_FG_BRIGHT_GREEN
* ANALOG_ANSI_FG_BRIGHT_YELLOW
* ANALOG_ANSI_FG_BRIGHT_BLUE
* ANALOG_ANSI_FG_BRIGHT_MAGENTA
* ANALOG_ANSI_FG_BRIGHT_CYAN
* ANALOG_ANSI_FG_BRIGHT_WHITE

### Background

* ANALOG_ANSI_BG_BLACK
* ANALOG_ANSI_BG_RED
* ANALOG_ANSI_BG_GREEN
* ANALOG_ANSI_BG_YELLOW
* ANALOG_ANSI_BG_BLUE
* ANALOG_ANSI_BG_MAGENTA
* ANALOG_ANSI_BG_CYAN
* ANALOG_ANSI_BG_WHITE
* ANALOG_ANSI_BG_BRIGHT_BLACK
* ANALOG_ANSI_BG_BRIGHT_RED
* ANALOG_ANSI_BG_BRIGHT_GREEN
* ANALOG_ANSI_BG_BRIGHT_YELLOW
* ANALOG_ANSI_BG_BRIGHT_BLUE
* ANALOG_ANSI_BG_BRIGHT_MAGENTA
* ANALOG_ANSI_BG_BRIGHT_CYAN
* ANALOG_ANSI_BG_BRIGHT_WHITE

### Miscellaneous

* ANALOG_ANSI_RESET
* ANALOG_ANSI_DEFAULT (use when you don't want a color but have to provide a color argument)

### Foreground (surround version)

* ANALOG_COLOR_FG_BLACK(str)
* ANALOG_COLOR_FG_RED(str)
* ANALOG_COLOR_FG_GREEN(str)
* ANALOG_COLOR_FG_YELLOW(str)
* ANALOG_COLOR_FG_BLUE(str)
* ANALOG_COLOR_FG_MAGENTA(str)
* ANALOG_COLOR_FG_CYAN(str)
* ANALOG_COLOR_FG_WHITE(str)
* ANALOG_COLOR_FG_BRIGHT_BLACK(str)
* ANALOG_COLOR_FG_BRIGHT_RED(str)
* ANALOG_COLOR_FG_BRIGHT_GREEN(str)
* ANALOG_COLOR_FG_BRIGHT_YELLOW(str)
* ANALOG_COLOR_FG_BRIGHT_BLUE(str)
* ANALOG_COLOR_FG_BRIGHT_MAGENTA(str)
* ANALOG_COLOR_FG_BRIGHT_CYAN(str)
* ANALOG_COLOR_FG_BRIGHT_WHITE(str)

### Background (surround version)

* ANALOG_COLOR_BG_BLACK(str)
* ANALOG_COLOR_BG_RED(str)
* ANALOG_COLOR_BG_GREEN(str)
* ANALOG_COLOR_BG_YELLOW(str)
* ANALOG_COLOR_BG_BLUE(str)
* ANALOG_COLOR_BG_MAGENTA(str)
* ANALOG_COLOR_BG_CYAN(str)
* ANALOG_COLOR_BG_WHITE(str)
* ANALOG_COLOR_BG_BRIGHT_BLACK(str)
* ANALOG_COLOR_BG_BRIGHT_RED(str)
* ANALOG_COLOR_BG_BRIGHT_GREEN(str)
* ANALOG_COLOR_BG_BRIGHT_YELLOW(str)
* ANALOG_COLOR_BG_BRIGHT_BLUE(str)
* ANALOG_COLOR_BG_BRIGHT_MAGENTA(str)
* ANALOG_COLOR_BG_BRIGHT_CYAN(str)
* ANALOG_COLOR_BG_BRIGHT_WHITE(str)

## Filtering

libanalog allows filtering through the `ANALOG_CONFIG_filter` global variable. Just set it to a pointer to a function with this signature:

```c
bool filter(char* log_type);
```

Example:

```c
bool debug_enabled = true;

bool my_filter(char* log_type)
	if (strcmp(log_type, "debug") == 0 && !debug_enabled) return false;
	return true;
}

int main(void) {
	ANALOG_CONFIG_filter = my_filter;

	ANALOGf_debug("This will be printed.\n");
	debug_enabled = false;
	ANALOGf_debug("This will not.\n");
	ANALOGf_info("But this will.\n");
	debug_enabled = true;
	ANALOGf_debug("This will, too.\n");
}
```

## Default target/ID

You can set the default target or the default ID through the `ANALOG_CONFIG_default_target` (`FILE*`) and `ANALOG_CONFIG_default_id` (`char*`) global variables.

The absolute default is `stderr` for the target and `log` for the ID. The absolute default is used when the variable is set to `NULL`, otherwise, its value is used. `ANALOG_CONFIG_default_target` is a file pointer, therefore it can also be set to point to a file (which will make libanalog write into the file).

Example:
	
```c
int main(void) {
    ANALOG_CONFIG_default_target = stderr;
    ANALOG_CONFIG_default_id = "hello";

    ANALOGf_debug("This will be printed into stderr with the ID hello.\n");
}
```

## Custom log types

You can extend libanalog by defining your own log types through the `ANALOG_DECLARE_LOG_TYPE` and `ANALOG_DEFINE_LOG_TYPE` macros, intended to be used at the top level. When used, these will define the usual set of 5 functions mentioned in "Normal usage" with the provided suffix and color.

* `ANALOG_DECLARE_LOG_TYPE` - 1 argument, `log_type` (string literal)
* `ANALOG_DEFINE_LOG_TYPE` - 3 arguments, `log_type` (string literal), `color` (ANSI color constant, for the log type name's color), `text_color` (ANSI color constant, for the message color)

Example:

```c
ANALOG_DECLARE_LOG_TYPE(my_log_type); // in a header file
ANALOG_DEFINE_LOG_TYPE(my_log_type, ANALOG_ANSI_FG_BLUE, ANALOG_ANSI_DEFAULT); // in a source file

int main(void) {
	// [...]
	ANALOGf_my_log_type("My own log type!\n");
	ANALOGitf_my_log_type("custom", stdout, "My own log type, written into stdout with a custom ID!\n");
}
```

## Changing log type colors without recompiling (and allowing users to change them)

Through adding the `-DANALOG_ENV_SUPPORT=1` flag to the `cmake` line while building, you can build a version of libanalog that allows configuring colors through `ANALOG_COLOR_X` and `ANALOG_TEXT_COLOR_X` env variables (where `X` is the log type).

Building:

```bash
mkdir build
cd build
cmake -DANALOG_ENV_SUPPORT=1 ..
make
```

Usage example:

```bash
env ANALOG_COLOR_warn=$'\x1b[31m' ANALOG_TEXT_COLOR_error= ./test/libanalog_test
```

## Disabling default log types

If you want, you can compile a version of libanalog that does not include any log types by default (but still allows you to define your own through the relevant macros). This is accomplished by adding `-DANALOG_DISABLE_DEFAULT_LOG_TYPES=1` to the `cmake` line while building, like this:

```bash
mkdir build
cd build
cmake -DANALOG_DISABLE_DEFAULT_LOG_TYPES=1 ..
make
```