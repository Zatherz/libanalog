#include <libanalog/analog.h>
#include <libanalog/colors.h>
#include <string.h>
#include <stdio.h>
#include "test.h"
#include <stdlib.h>
#include <limits.h>

ANALOG_DEFINE_LOG_TYPE(my_own_log_type, ANALOG_ANSI_FG_BRIGHT_BLACK, ANALOG_ANSI_DEFAULT);
ANALOG_DEFINE_LOG_TYPE(my_own_log_type_with_bg, ANALOG_ANSI_FG_BLACK ANALOG_ANSI_BG_WHITE, ANALOG_ANSI_DEFAULT);

bool my_filter(char* log_type) {
	// can't use ANALOG_* in here because we'd run into an infinite loop!
	if (strcmp(log_type, "debug") == 0) {
		return false;
	}

	return true;
}

int main(void) {
	ANALOGf_debug("Hello, %s!\n", "world");

	ANALOGf_debug("This is a 'debug' log line.\n");
	ANALOGf_error("This is an 'error' log line.\n");
	ANALOGf_warn("This is a 'warn' log line.\n");
	ANALOGf_info("This is an 'info' log line.\n");
	ANALOGf_my_own_log_type("This is a custom 'my_own_log_type' log line.\n");
	ANALOGf_my_own_log_type_with_bg("This is a custom 'my_own_log_type_with_bg' log line.\n");

	ANALOGf_info("Testing placeholders. 10: %d, 'a': %c, SSIZE_MAX: %zu\n", 10, 'a', SSIZE_MAX);

	ANALOGf_debug("Now, a custom filter will be set that will deny printing debug lines.\n");

	ANALOG_CONFIG_filter = my_filter;

	ANALOGf_debug("This will not be printed.\n");

	ANALOGf_error("Other log types still go through.\n");
	ANALOGf_my_own_log_type("Including custom log types.\n");

	ANALOG_CONFIG_filter = NULL;

	ANALOGf_debug("Removed custom filter.\n");

	ANALOGf_debug("If environment variable support is compiled in, the 'debug' string in the next message should be red (or at least a different color than before, if you're using some weird color scheme where red is not red). If not (or if you have already set the color to red through an environment variable yourself), it will remain the same color.\n");

	setenv("ANALOG_COLOR_debug", ANALOG_ANSI_FG_RED, true);
	ANALOGf_debug("Hi.\n");
	unsetenv("ANALOG_COLOR_debug");

	ANALOGf_debug("Customizing colors also works for custom log types.\n");

	setenv("ANALOG_COLOR_my_own_log_type", ANALOG_ANSI_FG_BRIGHT_RED, true);
	ANALOGf_my_own_log_type("Hi.\n");
	unsetenv("ANALOG_COLOR_my_own_log_type");

	ANALOGif_info("something", "This is a log line with a custom ID.\n");

	ANALOG_CONFIG_default_id = "analog test";
	ANALOGf_info("This log line was produced through a standard log call after setting the default_id.\n");
	ANALOGf_debug("Just like this one.\n");

	ANALOGtf_warn(stdout, "This log line was printed into stdout instead of stderr.\n");
	ANALOGitf_error("TEST", stdout, "This log line was printed into stdout and also has a custom ID.\n");

	ANALOG_CONFIG_default_id = NULL;

	ANALOGf_debug("Default ID has been reset.\n");

	ANALOG_CONFIG_default_target = stdout;

	ANALOGf_debug("All log lines starting from this one are now going to be printed to stdout as a result of setting default_target.\n");
	ANALOGf_info("Hi!\n");

	ANALOG_CONFIG_default_target = NULL;

	ANALOGf_debug("Default target has been reset, log lines are going to stderr again.\n");

	ANALOGf_debug("Just " ANALOG_COLOR_FG_RED("having") " fun " ANALOG_COLOR_FG_BRIGHT_CYAN("with") " colors...\n");
	ANALOGf_error("Note that as a " ANALOG_COLOR_FG_RED("result") " of how ANSI escapes work, using color surrounds inside colored messages will interrupt the original color.\n");
	ANALOGf_error("You can " ANALOG_ANSI_FG_RED "use" ANALOG_ANSI_FG_BRIGHT_YELLOW " the normal color constants, however, as they are constants...\n");

	ANALOGf_error("You can " ANALOG_ANSI_FG_RED "use" ANALOG_ANSI_FG_BRIGHT_YELLOW " the normal color constants and instead of resetting the color set it to what it was before, however, as they are constants...\n");
	setenv("ANALOG_TEXT_COLOR_error", ANALOG_ANSI_FG_BRIGHT_RED, true);
	ANALOGf_error("...they will " ANALOG_ANSI_FG_RED "break" ANALOG_ANSI_FG_BRIGHT_YELLOW " on custom text colors specified through env vars (you won't see this effect if you don't have env support compiled in).\n");
	unsetenv("ANALOG_TEXT_COLOR_error");
	ANALOGf_error("There is no proper way to do it, therefore using colors in log statements that you expect to be colored is not recommended.\n");
}