#ifndef LIBANALOG_TEST_H
#define LIBANALOG_TEST_H

#include <libanalog/analog.h>

ANALOG_DECLARE_LOG_TYPE(my_own_log_type);
ANALOG_DECLARE_LOG_TYPE(my_own_log_type_with_bg);

bool my_filter(char* log_type);
int main(void);

#endif//LIBANALOG_TEST_H